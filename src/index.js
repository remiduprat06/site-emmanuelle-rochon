import * as THREE from 'three';
import { Text } from 'troika-three-text'
import fonts from "./fonts";
import css from "./style.css";

const NB_MENU_ITEMS = 6;
const MENU_TEXTS = ["la", "truffade", "J'aime", " l'argent " , "mais", "surtout"];
const MENU_HREFS = ["href_1", "href_2", "href_3", "href_4" , "href_5", "href_6"]
const raycaster = new THREE.Raycaster();
const mouse = new THREE.Vector2();

var cubes = [];
var wireframes = [];
var cube_texts = []
var front_cube = null
var old_front_cube_color = "";
var updated_hovered_cube_color = 0;

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
scene.background = new THREE.Color( 0xAAAAAA )

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize( window.innerWidth, window.innerHeight );
let domElement = renderer.domElement
domElement.id = "canva3D"
document.body.appendChild( domElement );

const circle_geometry = new THREE.CircleGeometry(2, NB_MENU_ITEMS);
mat = new THREE.MeshBasicMaterial( {color: 0x00000000} )
const circle = new THREE.Mesh( circle_geometry, mat);
circle.geometry.rotateY(Math.PI / 2)
circle.geometry.translate(-5, 0, 0)
circle.visible = false
scene.add(circle)

// Cubes + Wireframes creation

for(let i=0; i < NB_MENU_ITEMS*3; i+=3){

    var cube_color = '#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');

    var geometry = new THREE.BoxGeometry(2.5,1,1);
    var material = new THREE.MeshBasicMaterial( { color: cube_color} );
    var cube = new THREE.Mesh( geometry, material );
    cube.userData.href = MENU_HREFS[i/3]
    scene.add( cube );

    var edges = new THREE.EdgesGeometry( cube.geometry )
    var mat = new THREE.LineBasicMaterial( { 
        color: 0x666666, 
        linewidth: 4,
        dashed: false,
        alphaToCoverage: true} )
    var wireframe = new THREE.LineSegments( edges, mat )
    scene.add(wireframe)

    const cube_text = new Text()
    scene.add(cube_text)

    // Set properties to configure:
    cube_text.font = fonts["Roboto"]
    cube_text.text = MENU_TEXTS[i/3]
    cube_text.fontSize = 0.4
    cube_text.material.opacity = 0.3
    cube_text.anchorX = 'center'
    cube_text.color = 0xFFFFFF - cube.material.color

    // Update the rendering:
    cube_text.sync()

    wireframe.visible = false
    cube.attach(wireframe)
    cube.attach(cube_text)

    cube.material.transparent = true;
    cube.material.opacity = 0.3

    cube.rotateY(0.3)
    wireframe.scale.set(1.002, 1.002, 1.002)
    
    cubes.push(cube)
    wireframes.push(wireframe)
    cube_texts.push(cube_text)
}

camera.position.z = 7; 

function animate() {                

    var new_menu_points = circle.geometry.attributes.position.array
    var start_index = 6

    for(let i=0; i<cubes.length*3; i+=3){
        
        let x = new_menu_points[start_index + i]
        let y = new_menu_points[start_index + i+1]
        let z = new_menu_points[start_index + i+2]

        cubes[i/3].position.set(x, y, z)

        let cube_text = cube_texts[i/3]
        cube_text.position.set(-(cube_text.fontSize/4), 0.25, 0.51)
        
    }

    update_front_cube();

    raycaster.setFromCamera( mouse, camera );
    // calculate objects intersecting the picking ray
	const intersects = raycaster.intersectObjects( scene.children );
    var intersected_front_cube = false;

    //Si il n'y a pas d'intersection avec FrontCube

    for ( let i = 0; i < intersects.length; i ++ ) {

        let object = intersects[ i ].object;

        if(object instanceof THREE.Mesh){
            if(object == front_cube) {
                intersected_front_cube = true
            }                       
        }
	}

    if(intersected_front_cube){
        $('body').css('cursor', 'pointer');
        if(updated_hovered_cube_color < 15){
            if(updated_hovered_cube_color == 0){
                old_front_cube_color = front_cube.material.color.getHexString()
            }
            if(front_cube.material.color){
                front_cube.material.color.add(new THREE.Color(0x020202));
            }
            updated_hovered_cube_color++;
        }
    } else {
        $('body').css('cursor', 'default');
        console.log();
        while(updated_hovered_cube_color > 0) {
            front_cube.material.color.sub(new THREE.Color(0x020202));
            updated_hovered_cube_color--
        }
    }

    requestAnimationFrame( animate );
    renderer.render( scene, camera );
    
}

function get_closest_cube(){
    var cube = cubes[0] 


    for(let i=0; i<cubes.length; i++){
        if(cubes[i].position.z > cube.position.z){
            cube = cubes[i]
        }
    }

    return cube

}

function update_front_cube(){
    var new_front_cube = get_closest_cube();

    if(front_cube == null || front_cube != new_front_cube){
        if(front_cube != null){
            front_cube.material.transparent = true
            front_cube.children[0].visible = false
            front_cube.children[1].material.transparent = true
            if(updated_hovered_cube_color > 0){
                front_cube.material.color.set("#" + old_front_cube_color)
                updated_hovered_cube_color = 0;
            }
            
        }        
        old_front_cube_color = "";
        front_cube = new_front_cube
    }

    front_cube.material.transparent = false
    front_cube.children[0].visible = true
    front_cube.children[1].material.transparent = false
    
}

$(window).on("mousewheel DOMMouseScroll", function(event) {

    if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {

        circle.geometry.rotateX(0.13)
    }
    else {

        circle.geometry.rotateX(-0.13)
    }

});


function onMouseMove( event ) {

	// calculate mouse position in normalized device coordinates
	// (-1 to +1) for both components

	mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

}

function onMouseClick( event ) {
    if($('body').css('cursor') == 'pointer' ){
        console.log(old_front_cube_color);
        $("#sideDiv").css("background-color", "#" + old_front_cube_color);
    }
}

window.addEventListener( 'mousemove', onMouseMove, false );
window.addEventListener( 'click', onMouseClick, false );

window.requestAnimationFrame(animate);

animate();